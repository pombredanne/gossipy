"""
Generate gossimon graphs using matplotlib
"""

import matplotlib.pyplot as plt
import numpy as np


class Graphs(object):
    """
    Generate graphs of gossimon cluster info
    """

    def __init__(self, cluster_info):
        self._ci = cluster_info
        self.default_item_color = "#9E3B33"
        self.default_stacked_colors = ["#E48743", "#81C6DD"]
        self.item_color = {}

    def _get_item_color(self, item, default_color):
        return getattr(self.item_color, item, default_color)

    def _plot_bars(self, plot_obj, nodes, item, ind, width):
        bars_labels = []
        bars_values = []
        for n in nodes:
            node_obj = self._ci.get_node(n)
            bars_values.append(getattr(node_obj, item))
        color = self._get_item_color(item, self.default_item_color)
        #print color
        plot_obj.bar(ind, bars_values, width=width, color=color)

    def plot_bars(self, nodes=None, item="load"):
        """
        Plot a bar graph of item values for given nodes
        """

        if nodes is None:
            nodes = self._ci.nodes
        plt.clf()
        width = 0.35
        bars_labels = []
        bars_index = np.arange(1, len(nodes)+1)
        for n in nodes:
            node_obj = self._ci.get_node(n)
            bars_labels.append(node_obj.pe)

        self._plot_bars(plt, nodes, item, bars_index, width)
        plt.xticks(bars_index + width / 2, bars_labels)
        return plt

    def _plot_stacked_bars(self, plot_obj, nodes, item1, item2, ind, width):
        bars_values = []
        bars_values2 = []
        for n in nodes:
            node_obj = self._ci.get_node(n)
            bars_values.append(getattr(node_obj, item1))
            bars_values2.append(getattr(node_obj, item2))

        color1 = self._get_item_color(item1, self.default_stacked_colors[0])
        color2 = self._get_item_color(item2, self.default_stacked_colors[1])
        plot_obj.bar(ind, bars_values, width=width, color=color1)
        plot_obj.bar(ind, bars_values2, bottom=bars_values, width=width, color=color2)

    def plot_stacked_bars(self, nodes=None, item1="used_ram", item2="free_ram"):
        if nodes is None:
            nodes = self._ci.nodes

        plt.clf()

        width = 0.35
        bars_labels = []
        for n in nodes:
            node_obj = self._ci.get_node(n)
            bars_labels.append(node_obj.pe)

        bars_index = np.arange(1, len(nodes)+1)
        self._plot_stacked_bars(plt, nodes, item1, item2, bars_index, width)
        plt.xticks(bars_index + width / 2, bars_labels)
        return plt

    def plot_side_bars(self, nodes=None, items1=["load"],
                       items2=["used_ram", "free_ram"],
                       item1_label="Load", item2_label="Memory"):
        if nodes is None:
            nodes = self._ci.nodes

        width = 0.35
        bars_labels = []
        for n in nodes:
            node_obj = self._ci.get_node(n)
            bars_labels.append(node_obj.pe)

        bars_index = np.arange(1, len(nodes)+1)

        plt.clf()
        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        if len(items1) == 1:
            self._plot_bars(ax1, nodes, items1[0], bars_index, width)
        else:
            self._plot_stacked_bars(ax1, nodes, items1[0], items1[1], bars_index, width)

        ax1.set_xlabel('Nodes')
        # Make the y-axis label and tick labels match the line color.
        ax1.set_ylabel(item1_label)
        #for tl in ax1.get_yticklabels():
        #    tl.set_color('b')


        ax2 = ax1.twinx()
        ax2.set_ylabel(item2_label)
        #for tl in ax2.get_yticklabels():
        #tl.set_color('r')

        if len(items2) == 1:
            self._plot_bars(ax2, nodes, items2[0], bars_index + width, width)
        else:
            self._plot_stacked_bars(ax2, nodes, items2[0], items2[1], bars_index + width, width)

        plt.xticks(bars_index + width, bars_labels)
        return plt
