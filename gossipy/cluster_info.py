"""
Provieds the ClusterInfo class
"""
import xmltodict


class NodeInfo(object):
    """
    Access to node information
    """
    def __init__(self, node):
        """
        Build the object from the provided node dictionary
        """
        self.name = node["name"]
        self.load = int(node["load"])
        self.ncpus = int(node["ncpus"])
        self.speed = int(node["speed"])
        self.pe = int(node["pe"])
        self.infod_status = int(node["infod_status"])
        self.dead_str = node["infod_dead_str"]
        #print node["tmem"]
        self.total_ram = (float(node["tmem"]["#text"]) * 4096.0) / (1024.0*1024.0)
        self.free_ram = (float(node["freepages"]) * 4096.0) / (1024.0*1024.0)
        self.used_ram = self.total_ram - self.free_ram

        self.kernel_version = node["kernel-version"]
        self.uptime = int(node["uptime"]["#text"])


class ClusterInfo(object):
    """
    Provides cluster information access methods.

    This class parse the xml obtained from the infod daemon and provides
    varous access methods
    """

    def __init__(self, xml):
        if xml is None:
            raise Exception("Must provide xml string")

        self.xml_dict = xmltodict.parse(xml)
        self.nodes = []
        self.nodes_dict = {}
        self.nodes_obj_list = []
        if isinstance(self.xml_dict["cluster_info"]["node"], (list, tuple)):
            for node in self.xml_dict["cluster_info"]["node"]:
                self._add_node(node)
        else:
            self._add_node(self.xml_dict["cluster_info"]["node"])

    def _add_node(self, node):
        """
        Utility method for adding a node
        """
        self.nodes.append(node["name"])
        node_obj = NodeInfo(node)
        self.nodes_obj_list.append(node_obj)
        self.nodes_dict[node_obj.name] = node_obj

    def get_node(self, name):
        return self.nodes_dict[name]
