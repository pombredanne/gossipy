import matplotlib as mpl
mpl.use('Agg')

import infod_client
import cluster_info
import graphs


infod_predefined_xml = """<?xml version="1.0" encoding="UTF-8" ?>
<cluster_info size="1" unit="4096">
	<node age="0.22">
		<name>r1</name>
		<pe>1</pe>
		<infod_status>1</infod_status>
		<infod_dead_str>alive</infod_dead_str>
		<external_status>no info</external_status>
		<tmem unit="4KB">929171</tmem>
		<tswap unit="4KB">5381631</tswap>
		<fswap unit="4KB">5377766</fswap>
		<uptime unit="Sec">122631</uptime>
		<disk_read_rate unit="KByte/Sec">0</disk_read_rate>
		<disk_write_rate unit="KByte/Sec">0</disk_write_rate>
		<net_rx_rate unit="KByte/Sec">0</net_rx_rate>
		<net_tx_rate unit="KByte/Sec">0</net_tx_rate>
		<tdisk unit="4KB">0</tdisk>
		<fdisk unit="4KB">0</fdisk>
		<locals>0</locals>
		<guests>0</guests>
		<maxguests>0</maxguests>
		<mosix_version>0</mosix_version>
		<total_freeze>0</total_freeze>
		<free_freeze>0</free_freeze>
		<arch_size>0</arch_size>
		<iowait>0</iowait>
		<extra_unused_2>0</extra_unused_2>
		<extra_unused_3>0</extra_unused_3>
		<extra_unused_4>0</extra_unused_4>
		<extra_unused_5>0</extra_unused_5>
		<nfs_client_rpc_rate unit="Ops/Sec">0</nfs_client_rpc_rate>
		<freepages>115817</freepages>
		<totalpages>0</totalpages>
		<load>8</load>
		<speed>11970</speed>
		<not_used_1>0</not_used_1>
		<status>1027</status>
		<level>0</level>
		<frozen>0</frozen>
		<util>0</util>
		<ncpus>4</ncpus>
		<nproc>0</nproc>
		<token_level>0</token_level>
		<edgepages>0</edgepages>
		<not_used_2>0</not_used_2>
		<baches>0</baches>
		<pad0>0</pad0>
		<pad1>458752</pad1>
		<pad2>2949120</pad2>
		<pad3>1919287308</pad3>
		<pad4>1702520165</pad4>
		<freeze-info stat="ok" total="0" free="0" />
		<usedby stat="ERROR"  />
		<proc-watch>error</proc-watch>
		<cid-crc>0</cid-crc>
		<infod-debug nchild="1">3573</infod-debug>
		<kernel-version>3.2.0-29-generic</kernel-version>
		<provider-type>linux</provider-type>
		<mosix-info>empty</mosix-info>
		<external>error</external>
	</node>
    <node age="0.22">
		<name>r2</name>
		<pe>2</pe>
		<infod_status>1</infod_status>
		<infod_dead_str>alive</infod_dead_str>
		<external_status>no info</external_status>
		<tmem unit="4KB">929171</tmem>
		<tswap unit="4KB">5381631</tswap>
		<fswap unit="4KB">5377766</fswap>
		<uptime unit="Sec">122631</uptime>
		<disk_read_rate unit="KByte/Sec">0</disk_read_rate>
		<disk_write_rate unit="KByte/Sec">0</disk_write_rate>
		<net_rx_rate unit="KByte/Sec">0</net_rx_rate>
		<net_tx_rate unit="KByte/Sec">0</net_tx_rate>
		<tdisk unit="4KB">0</tdisk>
		<fdisk unit="4KB">0</fdisk>
		<locals>0</locals>
		<guests>0</guests>
		<maxguests>0</maxguests>
		<mosix_version>0</mosix_version>
		<total_freeze>0</total_freeze>
		<free_freeze>0</free_freeze>
		<arch_size>0</arch_size>
		<iowait>0</iowait>
		<extra_unused_2>0</extra_unused_2>
		<extra_unused_3>0</extra_unused_3>
		<extra_unused_4>0</extra_unused_4>
		<extra_unused_5>0</extra_unused_5>
		<nfs_client_rpc_rate unit="Ops/Sec">0</nfs_client_rpc_rate>
		<freepages>115817</freepages>
		<totalpages>0</totalpages>
		<load>8</load>
		<speed>11970</speed>
		<not_used_1>0</not_used_1>
		<status>1027</status>
		<level>0</level>
		<frozen>0</frozen>
		<util>0</util>
		<ncpus>4</ncpus>
		<nproc>0</nproc>
		<token_level>0</token_level>
		<edgepages>0</edgepages>
		<not_used_2>0</not_used_2>
		<baches>0</baches>
		<pad0>0</pad0>
		<pad1>458752</pad1>
		<pad2>2949120</pad2>
		<pad3>1919287308</pad3>
		<pad4>1702520165</pad4>
		<freeze-info stat="ok" total="0" free="0" />
		<usedby stat="ERROR"  />
		<proc-watch>error</proc-watch>
		<cid-crc>0</cid-crc>
		<infod-debug nchild="1">3573</infod-debug>
		<kernel-version>3.2.0-29-generic</kernel-version>
		<provider-type>linux</provider-type>
		<mosix-info>empty</mosix-info>
		<external>error</external>
	</node>
    <node age="0.22">
		<name>r3</name>
		<pe>3</pe>
		<infod_status>1</infod_status>
		<infod_dead_str>alive</infod_dead_str>
		<external_status>no info</external_status>
		<tmem unit="4KB">929171</tmem>
		<tswap unit="4KB">5381631</tswap>
		<fswap unit="4KB">5377766</fswap>
		<uptime unit="Sec">122631</uptime>
		<disk_read_rate unit="KByte/Sec">0</disk_read_rate>
		<disk_write_rate unit="KByte/Sec">0</disk_write_rate>
		<net_rx_rate unit="KByte/Sec">0</net_rx_rate>
		<net_tx_rate unit="KByte/Sec">0</net_tx_rate>
		<tdisk unit="4KB">0</tdisk>
		<fdisk unit="4KB">0</fdisk>
		<locals>0</locals>
		<guests>0</guests>
		<maxguests>0</maxguests>
		<mosix_version>0</mosix_version>
		<total_freeze>0</total_freeze>
		<free_freeze>0</free_freeze>
		<arch_size>0</arch_size>
		<iowait>0</iowait>
		<extra_unused_2>0</extra_unused_2>
		<extra_unused_3>0</extra_unused_3>
		<extra_unused_4>0</extra_unused_4>
		<extra_unused_5>0</extra_unused_5>
		<nfs_client_rpc_rate unit="Ops/Sec">0</nfs_client_rpc_rate>
		<freepages>115817</freepages>
		<totalpages>0</totalpages>
		<load>8</load>
		<speed>11970</speed>
		<not_used_1>0</not_used_1>
		<status>1027</status>
		<level>0</level>
		<frozen>0</frozen>
		<util>0</util>
		<ncpus>4</ncpus>
		<nproc>0</nproc>
		<token_level>0</token_level>
		<edgepages>0</edgepages>
		<not_used_2>0</not_used_2>
		<baches>0</baches>
		<pad0>0</pad0>
		<pad1>458752</pad1>
		<pad2>2949120</pad2>
		<pad3>1919287308</pad3>
		<pad4>1702520165</pad4>
		<freeze-info stat="ok" total="0" free="0" />
		<usedby stat="ERROR"  />
		<proc-watch>error</proc-watch>
		<cid-crc>0</cid-crc>
		<infod-debug nchild="1">3573</infod-debug>
		<kernel-version>3.2.0-29-generic</kernel-version>
		<provider-type>linux</provider-type>
		<mosix-info>empty</mosix-info>
		<external>error</external>
	</node>
</cluster_info>
"""

xml = infod_client.get_xml_info(server='localhost')
#ci = cluster_info.ClusterInfo(infod_predefined_xml)
ci = cluster_info.ClusterInfo(xml)

print "{0:<20s} {1:<5s} {2:<5s} {3:<8s} {4:<8s} {5:<20s} {6:<8s}".format(
	"Name", "Load", "nCPUs", "TMEM", "FMEM", "Version", "Uptime")

for n in ci.nodes_obj_list:
	print "{0:<20s} {1:<5d} {2:<5d} {3:<8.1f} {4:<8.1f} {5:<20s} {6:<8d}".format(
		n.name, n.load, n.ncpus, n.total_ram, n.free_ram, n.kernel_version, n.uptime)

g = graphs.Graphs(ci)
p = g.plot_bars(item="load")
p.savefig("/tmp/load.png")

p = g.plot_stacked_bars(item1="used_ram", item2="free_ram")
p.savefig("/tmp/mem.png")


p = g.plot_side_bars(items1=["load"],
                     items2=["used_ram", "free_ram"],
                     item1_label="Load", item2_label="Memory")
p.savefig("/tmp/ml.png")
